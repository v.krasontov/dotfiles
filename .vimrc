" Automatically source vimrc on change
autocmd! bufwritepost *vimrc source %

set shell=/bin/bash
set nocompatible              " be iMproved, required
filetype off                  " required

" automatically download vim-plug
" Set GIT_SSL_NO_VERIFY to true, and use --insecure for curl to handle proxy
let $GIT_SSL_NO_VERIFY = 'true'
if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --insecure --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

let s:using_snippets = 0

call plug#begin('~/.vim/plugged')

  Plug 'posva/vim-vue'
  Plug 'pangloss/vim-javascript'
  " Plug 'Valloric/YouCompleteMe', { 'do': 'python3 install.py  --go-completer --ts-completer  ' }
  Plug 'scrooloose/nerdcommenter'
  Plug 'tpope/vim-cucumber'
  Plug 'airblade/vim-gitgutter'
  Plug 'fatih/vim-go'
  Plug 'OmniSharp/omnisharp-vim'
  Plug 'nickspoons/vim-sharpenup'
  Plug 'dense-analysis/ale'
  " Plug 'junegunn/fzf'
  " Plug 'junegunn/fzf.vim'
  Plug 'carlsmedstad/vim-bicep'
  Plug 'psf/black'

" Colorscheme
" Plug 'gruvbox-community/gruvbox'

" Statusline
" Plug 'itchyny/lightline.vim'
" Plug 'shinchu/lightline-gruvbox.vim'
" Plug 'maximbaz/lightline-ale'

" Snippet support
if s:using_snippets
  Plug 'sirver/ultisnips'
endif


  Plug 'leafgarland/typescript-vim'
  Plug 'peitalin/vim-jsx-typescript'

  Plug 'Shougo/deoplete.nvim'
  Plug 'roxma/nvim-yarp'
  Plug 'roxma/vim-hug-neovim-rpc'
  Plug 'Shougo/echodoc'

call plug#end()

if !exists('g:syntax_on') | syntax enable | endif

set completeopt=menuone,noinsert,noselect,popuphidden
set completepopup=highlight:Pmenu,border:off


" deoplete / completion stuff
 let g:deoplete#enable_at_startup = 1
 inoremap <silent><expr> <TAB>  pumvisible() ? "\<C-n>" : "\<TAB>"
 inoremap <silent><expr> <S-TAB>  pumvisible() ? "\<C-p>" : "\<S-TAB>"
 let g:echodoc#enablte_at_startup = 1
 " set cmdheight=2
 set completeopt+=popup,preview

" c-sharp omnisharp stuff
augroup omnisharp_commands
  autocmd!

  " Show type information automatically when the cursor stops moving.
  " Note that the type is echoed to the Vim command line, and will overwrite
  " any other messages in this space including e.g. ALE linting messages.
  autocmd CursorHold *.cs OmniSharpTypeLookup

  " The following commands are contextual, based on the cursor position.
  autocmd FileType cs nmap <silent> <buffer> gd <Plug>(omnisharp_go_to_definition)
  autocmd FileType cs nmap <silent> <buffer> <Leader>osfu <Plug>(omnisharp_find_usages)
  autocmd FileType cs nmap <silent> <buffer> <Leader>osfi <Plug>(omnisharp_find_implementations)
  autocmd FileType cs nmap <silent> <buffer> <Leader>pd <Plug>(omnisharp_preview_definition)
  autocmd FileType cs nmap <silent> <buffer> <Leader>ospi <Plug>(omnisharp_preview_implementations)
  autocmd FileType cs nmap <silent> <buffer> <Leader>ost <Plug>(omnisharp_type_lookup)
  autocmd FileType cs nmap <silent> <buffer> <Leader>osd <Plug>(omnisharp_documentation)
  autocmd FileType cs nmap <silent> <buffer> <Leader>osfs <Plug>(omnisharp_find_symbol)
  autocmd FileType cs nmap <silent> <buffer> <Leader>osfx <Plug>(omnisharp_fix_usings)
  autocmd FileType cs nmap <silent> <buffer> <C-\> <Plug>(omnisharp_signature_help)
  autocmd FileType cs imap <silent> <buffer> <C-\> <Plug>(omnisharp_signature_help)

  " Navigate up and down by method/property/field
  autocmd FileType cs nmap <silent> <buffer> [[ <Plug>(omnisharp_navigate_up)
  autocmd FileType cs nmap <silent> <buffer> ]] <Plug>(omnisharp_navigate_down)
  " Find all code errors/warnings for the current solution and populate the quickfix window
  autocmd FileType cs nmap <silent> <buffer> <Leader>osgcc <Plug>(omnisharp_global_code_check)
  " Contextual code actions (uses fzf, vim-clap, CtrlP or unite.vim selector when available)
  autocmd FileType cs nmap <silent> <buffer> <Leader>osca <Plug>(omnisharp_code_actions)
  autocmd FileType cs xmap <silent> <buffer> <Leader>osca <Plug>(omnisharp_code_actions)
  " Repeat the last code action performed (does not use a selector)
  autocmd FileType cs nmap <silent> <buffer> <Leader>os. <Plug>(omnisharp_code_action_repeat)
  autocmd FileType cs xmap <silent> <buffer> <Leader>os. <Plug>(omnisharp_code_action_repeat)

  autocmd FileType cs nmap <silent> <buffer> <Leader>os= <Plug>(omnisharp_code_format)

  autocmd FileType cs nmap <silent> <buffer> <Leader>osnm <Plug>(omnisharp_rename)

  autocmd FileType cs nmap <silent> <buffer> <Leader>osre <Plug>(omnisharp_restart_server)
  autocmd FileType cs nmap <silent> <buffer> <Leader>osst <Plug>(omnisharp_start_server)
  autocmd FileType cs nmap <silent> <buffer> <Leader>ossp <Plug>(omnisharp_stop_server)
augroup END


" general fold rules
set foldlevel=9 foldcolumn=0 foldnestmax=10 foldmethod=indent

set fileformat=unix

" general indent rules
set ts=2 sw=2 sts=0 expandtab

" special indent rules
augroup indentation_folding
  au!
  " always scan syntax from top to avoid weird highlighting
  au BufEnter * :syntax sync fromstart
  au Filetype javascript setlocal ts=2 sw=2 sts=0 expandtab foldmethod=syntax

  au BufNewFile,BufRead *.tsx,*.jsx set filetype=typescriptreact
  au BufNewFile,BufRead *.ts,*.tsx,*.jsx setlocal ts=2 sw=2 sts=0 et foldmethod=indent
  " below is an attempt to show 4 spaces as two - it don't work on all lines ?!
  " au BufNewFile,BufRead *.tsx,*.jsx syntax match spaces /  / conceal cchar= "Don't forget the space after cchar!
  " au BufNewFile,BufRead *.tsx,*.jsx set concealcursor=nvi conceallevel=1
  " au BufNewFile,BufRead *.tsx,*.jsx highlight conceal ctermbg=lightgrey guibg=lightgrey

  au Filetype cs set foldnestmax=9 et

  au Filetype vue setlocal ts=2 sw=2 sts=0 expandtab foldmethod=indent
  au Filetype html setlocal ts=2 sw=2 sts=0 expandtab foldmethod=syntax
  au Filetype css setlocal ts=2 sw=2 sts=0 noexpandtab foldmethod=syntax
  au Filetype cs setlocal tabstop=4 shiftwidth=4 softtabstop=4 expandtab
  au BufRead,BufNewFile *.go setlocal noet ts=4 sw=4 foldnestmax=8 foldlevel=9
    \ foldmethod=syntax
  au BufRead,BufNewFile *.lua setlocal ts=2 sw=2 sts=0 foldnestmax=8 foldlevel=9
    \ foldmethod=indent
  au InsertLeave *.go setlocal foldmethod=syntax
  au InsertEnter *.go setlocal foldmethod=manual
  au BufRead,BufNewFile *.csv,*.tsv,*.json setlocal textwidth=0
  au BufRead,BufNewFile *.tex setlocal ts=2 sw=2 foldnestmax=3 foldlevel=4
    \ foldmethod=indent expandtab
  au BufNewFile,BufRead *.yaml,*.yml,*.yml.j2,*.yaml.j2 setlocal
    \ foldmethod=indent ts=2 sw=2 sts=0 expandtab foldnestmax=10
    \ cindent cinkeys-=0# indentkeys-=0#
  au BufNewFile,BufRead *.py setlocal tabstop=4 softtabstop=4 shiftwidth=4
    \ textwidth=79 expandtab autoindent foldmethod=indent
  au BufNewFile,BufRead /tmp/mutt* set noautoindent filetype=mail wm=0 tw=78 nonumber digraph nolist
  au BufNewFile,BufRead ~/tmp/mutt* set noautoindent filetype=mail wm=0 tw=78 nonumber digraph nolist
augroup END

" COLORS
augroup highlighting
  au!
  au ColorScheme * highlight bws ctermbg=red guibg=red
  au ColorScheme * highlight todo ctermbg=lightyellow guibg=lightyellow
  au BufRead,BufWrite,BufEnter * let w:m1=matchadd('bws', '\s\+$', -1)  
  au BufRead,BufWrite,BufEnter * let w:m2=matchadd('todo', 'TODO', -1)
augroup END
" set general color scheme
colorscheme peachpuff
" change highlighting of matching parantheses (and reset background of
" git-gutter column)
highlight MatchParen cterm=bold ctermbg=lightgrey ctermfg=magenta
highlight clear SignColumn

" ale linting config
let g:ale_linters = {
      \ 'cs': ['OmniSharp']
      \}

" set colors for folded lines
" ssh onto machine, using putty in windows config
map <F6> :highlight Folded ctermfg=2 ctermbg=lightgrey<CR>
" native machine config
map <F7> :highlight Folded ctermfg=white ctermbg=0<CR>
highlight Folded ctermfg=2 ctermbg=lightgrey

" color scheme for yaml
augroup colors
  au!
  au BufNewFile,BufRead *.yaml,*.yml,*.yml.j2,*.yaml.j2 so ~/.vim/yaml.vim
augroup END

" MISCELLANEOUS
let g:go_fmt_experimental=1 " fixes closing folds on write in go
let g:go_fmt_command = "goimports"

" python stuff
let python_highlight_all=1
augroup python
  au!
  autocmd BufWritePre *.py execute ':Black'
augroup END

" disable arrow keys
noremap <Up> <NOP>
noremap <Down> <NOP>
noremap <Left> <NOP>
noremap <Right> <NOP>

" show ruler
set ruler

" set text width to 80
set tw=79

" use space for toggling fold status
nnoremap <Space> za

" set split directions
set splitbelow
set splitright

" let backspace delete autoindentation and across lines
set backspace=eol,indent,start

set encoding=utf-8

" disable visual bell
set vb
set t_vb=

" Better command-line completion
set wildmenu
" Complete like most shells – longest substring first, then iterate through
"  full matches
set wildmode=longest:full,full

" Show partial commands in the last line of the screen
set showcmd

" Do not highlight searches by default
set nohlsearch

" Backups

" don't keep swp files
set noswapfile

"------------------------------------------------------------
" Usability options
"
" These are options that users frequently set in their .vimrc. Some of them
" change Vim's behaviour in ways which deviate from the true Vi way, but
" which are considered to add usability. Which, if any, of these options to
" use is very much a personal preference, but they are harmless.

" Use case insensitive search, except when using capital letters
set ignorecase
set smartcase

" Replace all matches on the line, not just the first,
"  by default, without requiring …/g
set gdefault

" Incremental search
set incsearch

" Set the search scan to wrap around the file
set wrapscan

" Always display the status line, even if only one window is displayed
set laststatus=2

" Use <F11> to toggle between 'paste' and 'nopaste'
set pastetoggle=<F11>

" Session stuff

function! MakeSession()
  let b:sessiondir = $HOME . "/.vim/sessions" . getcwd()
  if (filewritable(b:sessiondir) != 2)
    exe 'silent !mkdir -p ' b:sessiondir
    redraw!
  endif
  let b:filename = b:sessiondir . '/session.vim'
  exe "mksession! " . b:filename
endfunction

function! LoadSession()
  let b:sessiondir = $HOME . "/.vim/sessions" . getcwd()
  let b:sessionfile = b:sessiondir . "/session.vim"
  if (filereadable(b:sessionfile))
    exe 'source ' b:sessionfile
  else
    echo "No session loaded."
  endif
endfunction

nmap <C-M-L> nested :call LoadSession()
inoremap <silent><expr> <TAB>  pumvisible() ? "\<C-n>" : "\<TAB>"
" Adding automatons for when entering or leaving Vim
augroup sessions
  autocmd!
  " au VimEnter * nested :call LoadSession()
  au VimLeave * :call MakeSession()
augroup END
