#!/bin/bash

action=$1

if [ "$action" == "down" ]; then
  /home/val/.local/bin/adjust-backlight.sh -10
  exit 0
fi
if [ "$action" == "up" ]; then
  /home/val/.local/bin/adjust-backlight.sh +10
  exit 0
fi
echo unexpected argument, aborting
exit 1
