# Dotfiles

This project contains a bunch of config files used on linux systems. Setting up
several machines in the same way can be achieved thusly.

## Installation

All the files in here are relative to `$HOME`. If you want to put them
elsewhere, you will have to adjust the below commands

Inside the project folder, run

```
# create folders and links in home directory
find . -type d -a ! -regex '.*\.git.*' -a ! -regex '^\.\/r00t.*' \
  -a ! -name '.' -exec mkdir -p "$HOME/{}" \;
find . ! -type d -a ! -name 'readme.md' -a ! -path './.git/*' \
  -a ! -regex '^\.\/r00t.*' -exec ln -s "$PWD/{}" "$HOME/{}" \;
# create folders and links under root, outside of home
# TODO have to remove r00t from the file path
find r00t -type d -a ! -regex '.*\.git.*' \
  -a ! -name 'r00t' -exec \
  sudo sh -c 'mkdir -p "${0#r00t}"' {} \;
find r00t ! -type d -exec \
  sudo sh -c 'ln -s "$PWD/${0}" "${0#r00t}"' {} \;
```

## Prerequisites

For adjusting the backlight, you should make sure that you're user is added to
the video group:

```
sudo addgroup [user] video
```

and login again.
