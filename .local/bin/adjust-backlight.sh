#!/bin/bash

if [ -z "$(groups | grep video)" ]; then
  echo 'user not in group `video`, aborting'
  exit 1
fi

adjust=$1
sign=$(echo $adjust | cut -b 1)
percentage=$(echo $adjust | cut -b 2-)
echo got \`$adjust\` as 1st argument
echo parsed as sign \`$sign\` and percentage \`$percentage\`

if ! [[ $adjust =~ ^[+-][0-9]+$ ]]; then
  echo expected percentage \`^[+-][0-9]+$\` as first argument - aborting
  exit 1
fi

backlight_dir=$(ls -d /sys/class/backlight/* | head -n1)
echo found first backlight: $backlight_dir

current_light=$(cat $backlight_dir/brightness)
echo current level is \`$current_light\`
max_light=$(cat ${backlight_dir}/max_brightness)
echo max level is \`$max_light\`

let amount=$max_light*$percentage/100

eval let new_light="$current_light$sign$amount"

if [ $new_light -le 0 ]; then
  new_light=0
fi
if [ $new_light -ge $max_light ]; then
  new_light=$max_light
fi

echo computed new light to be \`$new_light\`
echo $new_light > ${backlight_dir}/brightness
