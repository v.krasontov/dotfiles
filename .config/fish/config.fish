set local_bin $HOME/.local/bin
if not contains $local_bin $PATH
  set -gx PATH $local_bin $PATH
end

set -Ux LC_ALL en_US.UTF-8

set -Ux GOPATH ~/code/go
set GOBIN $GOPATH/bin

set cisco_vpn_bin /opt/cisco/anyconnect/bin
if not contains $cisco_vpn_bin $PATH
  set -gx PATH $PATH $cisco_vpn_bin
end

if not contains $GOBIN $PATH
  set -gx PATH $GOBIN $PATH
end

set ANDROIDBIN $HOME/adb-fastboot/platform-tools

if not contains $ANDROIDBIN $PATH
  set -gx PATH $ANDROIDBIN $PATH
end

set -gx DOTNET_ROOT /snap/dotnet-sdk/current
if not contains $DOTNET_ROOT $PATH
  set -gx PATH $DOTNET_ROOT $PATH
end

#if [ "$TERM" = "linux" ]
#echo -en "\e]PB839496" # S_base00
#  echo -en "\e]PA93a1a1" # S_base01
#  echo -en "\e]P0eee8d5" # S_base02
#  echo -en "\e]P62aa198" # S_cyan
#  echo -en "\e]P8fdf6e3" # S_base03
#  echo -en "\e]P2859900" # S_green
#  echo -en "\e]P5d33682" # S_magenta
#  echo -en "\e]P1dc322f" # S_red
#  echo -en "\e]PC657b83" # S_base0
#  echo -en "\e]PE586e75" # S_base1
#  echo -en "\e]P9cb4b16" # S_orange
#  echo -en "\e]P7073642" # S_base2
#  echo -en "\e]P4268bd2" # S_blue
#  echo -en "\e]P3b58900" # S_yellow
#  echo -en "\e]PF002b36" # S_base3
#  echo -en "\e]PD6c71c4" # S_violet
#  clear # against bg artifacts
#  set fish_color_autosuggestion bryellow
#end
#

set -gx PYTHON_KEYRING_BACKEND keyring.backends.null.Keyring

set -gx a10 7C:96:D2:8B:C0:76
set -gx bose 2C:41:A1:50:1C:6F
set -gx a5 40:EF:4C:1D:37:F0
set -gx shure 00:0E:DD:70:1F:20
set -gx mxmouse DD:D6:31:4B:23:F2

# Device DD:D6:31:4B:23:F1 MX Master 3 B
# Device F4:B6:88:72:C3:7F Poly V4220 Series

set -gx SSH_ENV "$HOME/.ssh/agent-environment"

function start_agent
    echo "Initialising new SSH agent..."
    /usr/bin/ssh-agent -c | sed 's/^echo/#echo/' > "$SSH_ENV"
    echo succeeded
    chmod 600 "$SSH_ENV"
    . "$SSH_ENV" > /dev/null
    /usr/bin/ssh-add;
end


function setup_ssh
  if [ -f "$SSH_ENV" ];
      . "$SSH_ENV" > /dev/null
      #ps $SSH_AGENT_PID doesn't work under cywgin
      ps -ef | grep $SSH_AGENT_PID | grep -e 'ssh-agent -c$' > /dev/null || start_agent;
  else
      start_agent;
  end
end

if status is-interactive
    setup_ssh
end

# fix folder history missing key bindings
bind --preset \eD prevd-or-backward-word
bind --preset \eB nextd-or-forward-word
