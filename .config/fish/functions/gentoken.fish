function gentoken
  gpg -dq $argv[1] | oathtool --base32 --totp -
end
