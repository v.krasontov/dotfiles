-- awesome_mode: screen=on
-- {{{ Required libraries
local awesome, client, mouse, screen, tag = awesome, client, mouse, screen, tag
local ipairs, string, os, table, tostring, tonumber, type = ipairs, string, os, table, tostring, tonumber, type

local gears         = require("gears")
local awful         = require("awful")
require("awful.autofocus")
local wibox         = require("wibox")
local beautiful     = require("beautiful")
local naughty       = require("naughty")
local menubar       = require("menubar")
local hotkeys_popup = require("awful.hotkeys_popup").widget
local my_table      = awful.util.table or gears.table -- 4.{0,1} compatibility
-- }}}


-- stuff for several monitors
local xrandr = require("xrandr")

-- reliably translate tags when switching screens (as in monitors)

--[[
-- disconnect standard handler
screen.disconnect_signal("request::remove", awful.screen.remove_screen_handler)

-- attach custom handler
tag.connect_signal("request::remove", function (viewport)
    local geo = viewport.geometry
    -- get to be removed screen
    local removed_screen
    for s in screen do
        if gears.geometry.rectangle.are_equal(geo, s.geometry) then
            removed_screen = s
            break
        end
    end
    -- Find another screen to move the clients to, and move them
    for target_screen in screen do
        -- Any screen that is not the to be removed screen will do
        if target_screen ~= removed_screen then
            -- loop through tags all tags on
            for source_tag in removed_screen.tags do
                -- Use a tag with the same name on the new screen
                local target_tag = awful.tag.find_by_name(target_screen, source_tag.name)
                if target_tag == nil then
                    -- The new screen doesn't have a tag with the same name -> chose a fallback tag
                    target_tag = awful.tag.find_fallback(target_screen)
                end

                -- Move all clients to the new tag
                for _, client in ipairs(source_tag:clients()) do
                    client:move_to_tag(target_tag)
                end
            end
            break
        end
    end
    removed_screen:fake_remove()
    return
end)
--]]

local blacklisted_snid = setmetatable({}, {__mode = "v" })

--- Make startup notification work for some clients like XTerm. This is ugly
-- but works often enough to be useful.
local function fix_startup_id(c)
  -- Prevent "broken" sub processes created by <code>c</code> to inherit its SNID
  if c.startup_id then
      blacklisted_snid[c.startup_id] = blacklisted_snid[c.startup_id] or c
      return
  end

  if not c.pid then return end

  naughty.notify{ text = "got pid " .. c.pid }

  -- Read the process environment variables
  local f = io.open("/proc/"..c.pid.."/environ", "rb")

  -- It will only work on Linux, that's already 99% of the userbase.
  if not f then return end

  local value = _VERSION <= "Lua 5.1" and "([^\z]*)\0" or "([^\0]*)\0"
  local snid = f:read("*all"):match("STARTUP_ID=" .. value)
  f:close()

  -- If there is already a client using this SNID, it means it's either a
  -- subprocess or another window for the same process. While it makes sense
  -- in some case to apply the same rules, it is not always the case, so
  -- better doing nothing rather than something stupid.
  if blacklisted_snid[snid] then return end

  c.startup_id = snid

  blacklisted_snid[snid] = c
end

--[[
awful.rules.add_rule_source(
  "snid",
  fix_startup_id,
  {},
  {"awful.spawn", "awful.rules"}
)
--]]

awful.util.shell = "/bin/bash"

-- {{{ Error handling
if awesome.startup_errors then
  naughty.notify({ preset = naughty.config.presets.critical,
  title = "Oops, there were errors during startup!",
  text = awesome.startup_errors })
end

do
  awesome.connect_signal("debug::error", function (err)
    if in_error then return end
    in_error = true

    naughty.notify({ preset = naughty.config.presets.critical,
    title = "Oops, an error happened!",
    text = tostring(err) })
    in_error = false
  end)
end
-- }}}

-- {{{ Autostart windowless processes

-- This function will run once every time Awesome is started, doing a fuzzy
-- check of the command
local function run_once_fuzzy(cmd, check_cmd)
  awful.spawn.with_shell(string.format("pgrep -u $USER '%s' > /dev/null || (%s)", check_cmd, cmd))
end
-- This function will run once every time Awesome is started, doing a precise
-- check for the command
local function run_once(cmd_arr)
  for _, cmd in ipairs(cmd_arr) do
    awful.spawn.with_shell(string.format("pgrep -u $USER -fx '%s' > /dev/null || (%s)", cmd, cmd))
  end
end

-- }}}

-- {{{ Variable definitions

local themes = {
  "blackburn",       -- 1
  "copland",         -- 2
  "dremora",         -- 3
  "holo",            -- 4
  "multicolor",      -- 5
  "powerarrow",      -- 6
  "powerarrow-dark", -- 7
  "rainbow",         -- 8
  "steamburn",       -- 9
  "vertex",          -- 10
}

local chosen_theme = themes[5]
local modkey       = "Mod4"
local altkey       = "Mod1"
local terminal     = "terminator"
local editor       = os.getenv("EDITOR") or "vim"
local editor_cmd = terminal .. " -e " .. editor
local gui_editor   = "gvim"
local browser      = "google-chrome"
local guieditor    = "atom"
local scrlocker    = "xscreensaver-command -lock"
local mediaplayer  = "vlc"

awful.util.terminal = terminal
awful.util.tagnames = { "1", "2", "3", "4", "5", "6", "7", "8", "9" }
awful.layout.layouts = {
  awful.layout.suit.floating,
  awful.layout.suit.tile,
  awful.layout.suit.tile.left,
  awful.layout.suit.tile.bottom,
  awful.layout.suit.tile.top,
  --awful.layout.suit.fair,
  --awful.layout.suit.fair.horizontal,
  --awful.layout.suit.spiral,
  --awful.layout.suit.spiral.dwindle,
  --awful.layout.suit.max,
  --awful.layout.suit.max.fullscreen,
  --awful.layout.suit.magnifier,
  --awful.layout.suit.corner.nw,
  --awful.layout.suit.corner.ne,
  --awful.layout.suit.corner.sw,
  --awful.layout.suit.corner.se,
}
awful.util.taglist_buttons = my_table.join(
  awful.button({ }, 1, function(t) t:view_only() end),
  awful.button({ modkey }, 1, function(t)
    if client.focus then
      client.focus:move_to_tag(t)
    end
  end),
  awful.button({ }, 3, awful.tag.viewtoggle),
  awful.button({ modkey }, 3, function(t)
    if client.focus then
      client.focus:toggle_tag(t)
    end
  end),
  awful.button({ }, 4, function(t) awful.tag.viewnext(t.screen) end),
  awful.button({ }, 5, function(t) awful.tag.viewprev(t.screen) end)
)
awful.util.tasklist_buttons = my_table.join(
  awful.button({ }, 1, function (c)
    if c == client.focus then
      c.minimized = true
    else
      -- Without this, the following
      -- :isvisible() makes no sense
      c.minimized = false
      if not c:isvisible() and c.first_tag then
        c.first_tag:view_only()
      end
      -- This will also un-minimize
      -- the client, if needed
      client.focus = c
      c:raise()
    end
  end),
  awful.button({ }, 3, function()
    local instance = nil

    return function ()
      if instance and instance.wibox.visible then
        instance:hide()
        instance = nil
      else
        instance = awful.menu.clients({ theme = { width = 250 } })
      end
    end
  end),
  awful.button({ }, 4, function ()
    awful.client.focus.byidx(1)
  end),
  awful.button({ }, 5, function ()
    awful.client.focus.byidx(-1)
end))

local theme_path = string.format("%s/.config/awesome/themes/%s/theme.lua", os.getenv("HOME"), chosen_theme)
beautiful.init(theme_path)
-- }}}

-- {{{ Menu
local myawesomemenu = {
   { "hotkeys", function() return false, hotkeys_popup.show_help end},
   { "manual", terminal .. " -e man awesome" },
   { "edit config", editor_cmd .. " " .. awesome.conffile },
   { "restart", awesome.restart },
   { "quit", function() awesome.quit() end}
}

local menu_awesome = { "awesome", myawesomemenu, beautiful.awesome_icon }
local menu_terminal = { "open terminal", terminal }

awful.util.mymainmenu = awful.menu({
  items = {
    menu_awesome,
    menu_terminal,
  }
})
menubar.utils.terminal = terminal
-- }}}

-- {{{ Screen
-- Re-set wallpaper when a screen's geometry changes (e.g. different resolution)
screen.connect_signal("property::geometry", function(s)
  -- Wallpaper
  if beautiful.wallpaper then
    local wallpaper = beautiful.wallpaper
    -- If wallpaper is a function, call it with the screen
    if type(wallpaper) == "function" then
      wallpaper = wallpaper(s)
    end
    gears.wallpaper.maximized(wallpaper, s, true)
  end
end)
-- Create a wibox for each screen and add it
awful.screen.connect_for_each_screen(function(s) beautiful.at_screen_connect(s) end)
-- }}}

-- {{{ Mouse bindings
root.buttons(my_table.join(
awful.button({ }, 3, function () awful.util.mymainmenu:toggle() end),
awful.button({ }, 4, awful.tag.viewnext),
awful.button({ }, 5, awful.tag.viewprev)
))
-- }}}

-- {{{ Key bindings
globalkeys = my_table.join(
  -- Take a screenshot
  -- https://github.com/lcpz/dots/blob/master/bin/screenshot
  awful.key({ altkey }, "p", function() os.execute("screenshot") end,
  {description = "take a screenshot", group = "hotkeys"}),

  -- X screen locker
  awful.key({ modkey, "Control" }, "l", function () os.execute(scrlocker) end,
  {description = "lock screen", group = "hotkeys"}),

  -- Brightness - figure out key names with xev
  awful.key({ }, "XF86Calculator", function ()
    awful.spawn("bash -c '" .. scrlocker .. "; systemctl suspend'") end),
  --  awful.spawn("echo '" .. scrlocker .. "; and systemctl suspend' >> /home/val/.aw-debug") end),
  awful.key({ }, "XF86MonBrightnessDown", function ()
    awful.spawn("cbacklight --dec 5") end),
  awful.key({ }, "XF86MonBrightnessUp", function ()
    awful.spawn("cbacklight --inc 5") end),
  awful.key({ }, "XF86KbdLightOnOff", function ()
    awful.spawn("sudo kdb-backlight") end),
  -- Hotkeys
  awful.key({ modkey,           }, "s",      hotkeys_popup.show_help,
    {description = "show help", group="awesome"}),
  -- Tag browsing
  awful.key({ modkey, "Control" }, "Left",   awful.tag.viewprev,
    {description = "view previous", group = "tag"}),
  awful.key({ modkey, "Control" }, "Right",  awful.tag.viewnext,
    {description = "view next", group = "tag"}),
  awful.key({ modkey, "Control" }, "Home",   awful.tag.viewprev,
    {description = "view previous", group = "tag"}),
  awful.key({ modkey, "Control" }, "End",  awful.tag.viewnext,
    {description = "view next", group = "tag"}),
  awful.key({ modkey,           }, "Escape", awful.tag.history.restore,
    {description = "go back", group = "tag"}),
  awful.key({ }, "XF86AudioLowerVolume", function () awful.spawn("pactl set-sink-volume @DEFAULT_SINK@ -5%") end),
  awful.key({ }, "XF86AudioRaiseVolume", function () awful.spawn("pactl set-sink-volume @DEFAULT_SINK@ +5%") end),
  awful.key({ }, "XF86AudioMute", function () awful.spawn("amixer -D pulse sset Master 1+ toggle") end),
  awful.key({ }, "XF86AudioMicMute", function () awful.spawn("pactl set-source-mute @DEFAULT_SOURCE@ toggle") end),
  awful.key({ modkey, "Shift" }, "t", function () awful.spawn("toggle-bluetooth-profile") end),
  awful.key({ modkey, altkey }, "3", function () awful.spawn("pactl set-sink-volume @DEFAULT_SINK@ -5%") end),
  awful.key({ modkey, altkey }, "4", function () awful.spawn("pactl set-sink-volume @DEFAULT_SINK@ +5%") end),
  awful.key({ modkey, altkey }, "m", function () awful.spawn("amixer -D pulse sset Master 1+ toggle") end),
  awful.key({ modkey, "Control" }, "s", function() xrandr.xrandr() end),
  awful.key({ modkey, }, "k", function() awful.spawn("killall redshift") end),


  -- Default client focus
  awful.key({ altkey,           }, "j",
    function () awful.client.focus.byidx( 1) end,
    {description = "focus next by index", group = "client"}),
  awful.key({ altkey,           }, "k",
    function () awful.client.focus.byidx(-1) end,
    {description = "focus previous by index", group = "client"}),

  -- By direction client focus
  awful.key({ modkey }, "j",
    function()
      awful.client.focus.global_bydirection("down")
      if client.focus then client.focus:raise() end
    end,
    {description = "focus down", group = "client"}),
  awful.key({ modkey }, "k",
    function()
      awful.client.focus.global_bydirection("up")
      if client.focus then client.focus:raise() end
    end,
    {description = "focus up", group = "client"}),
  awful.key({ modkey }, "h",
    function()
      awful.client.focus.global_bydirection("left")
      if client.focus then client.focus:raise() end
    end,
    {description = "focus left", group = "client"}),
  awful.key({ modkey }, "l",
    function()
      awful.client.focus.global_bydirection("right")
      if client.focus then client.focus:raise() end
    end,
    {description = "focus right", group = "client"}),
  awful.key({ modkey,           }, "w", function () awful.util.mymainmenu:show() end,
    {description = "show main menu", group = "awesome"}),

  -- Layout manipulation
  awful.key({ modkey, "Shift"   }, "j", function () awful.client.swap.byidx(  1)    end,
    {description = "swap with next client by index", group = "client"}),
  awful.key({ modkey, "Shift"   }, "k", function () awful.client.swap.byidx( -1)    end,
    {description = "swap with previous client by index", group = "client"}),
  awful.key({ modkey, "Control" }, "j", function () awful.screen.focus_relative( 1) end,
    {description = "focus the next screen", group = "screen"}),
  awful.key({ modkey, "Control" }, "k", function () awful.screen.focus_relative(-1) end,
    {description = "focus the next screen", group = "screen"}),
  awful.key({ altkey,           }, "`", function () awful.screen.focus_relative( 1) end,
    {description = "focus the next screen", group = "screen"}),
  awful.key({ modkey,           }, "`", function () awful.screen.focus_relative( 1) end,
    {description = "focus the previous screen", group = "screen"}),
  awful.key({ modkey,           }, "u", awful.client.urgent.jumpto,
    {description = "jump to urgent client", group = "client"}),
  awful.key({ modkey,           }, "Tab",
    function ()
      awful.client.focus.byidx( 1)
      if client.focus then client.focus:raise() end
    end,
    {description = "next client", group = "client"}),
  awful.key({ modkey, "Shift"   }, "Tab",
    function ()
      awful.client.focus.byidx(-1)
      if client.focus then client.focus:raise() end
    end,
    {description = "previous client", group = "client"}),

  -- Show/Hide Wibox
  awful.key({ modkey }, "b",
    function ()
      for s in screen do
        s.mywibox.visible = not s.mywibox.visible
        if s.mybottomwibox then
          s.mybottomwibox.visible = not s.mybottomwibox.visible
        end
      end
    end,
    {description = "toggle wibox", group = "awesome"}),

  -- Standard program
  awful.key({ modkey,           }, "Return", function () awful.spawn(terminal) end,
    {description = "open a terminal", group = "launcher"}),
  awful.key({ modkey, "Control" }, "r", awesome.restart,
    {description = "reload awesome", group = "awesome"}),
  awful.key({ modkey, "Shift"   }, "q", awesome.quit,
    {description = "quit awesome", group = "awesome"}),

  awful.key({ altkey, "Shift"   }, "l",     function () awful.tag.incmwfact( 0.05)          end,
    {description = "increase master width factor", group = "layout"}),
  awful.key({ altkey, "Shift"   }, "h",     function () awful.tag.incmwfact(-0.05)          end,
    {description = "decrease master width factor", group = "layout"}),
  awful.key({ modkey, "Shift"   }, "h",     function () awful.tag.incnmaster( 1, nil, true) end,
    {description = "increase the number of master clients", group = "layout"}),
  awful.key({ modkey, "Shift"   }, "l",     function () awful.tag.incnmaster(-1, nil, true) end,
    {description = "decrease the number of master clients", group = "layout"}),
  awful.key({ modkey, "Control" }, "h",     function () awful.tag.incncol( 1, nil, true)    end,
    {description = "increase the number of columns", group = "layout"}),
  awful.key({ modkey, "Control" }, "l",     function () awful.tag.incncol(-1, nil, true)    end,
    {description = "decrease the number of columns", group = "layout"}),
  awful.key({ modkey,           }, "space", function () awful.layout.inc( 1)                end,
    {description = "select next", group = "layout"}),
  awful.key({ modkey, "Shift"   }, "space", function () awful.layout.inc(-1)                end,
    {description = "select previous", group = "layout"}),

  awful.key({ modkey, "Control" }, "n",
    function ()
      local c = awful.client.restore()
      -- Focus restored client
      if c then
        client.focus = c
        c:raise()
      end
    end,
    {description = "restore minimized", group = "client"}),

  -- Dropdown application
  awful.key({ modkey, }, "z", function () awful.screen.focused().quake:toggle() end,
    {description = "dropdown application", group = "launcher"}),

  -- Copy primary to clipboard (terminals to gtk)
  awful.key({ modkey }, "c", function () awful.spawn("xsel | xsel -i -b") end,
    {description = "copy terminal to gtk", group = "hotkeys"}),
  -- Copy clipboard to primary (gtk to terminals)
  awful.key({ modkey }, "v", function () awful.spawn("xsel -b | xsel") end,
    {description = "copy gtk to terminal", group = "hotkeys"}),

  -- User programs
  awful.key({ modkey }, "q", function () awful.spawn(browser) end,
    {description = "run browser", group = "launcher"}),
  awful.key({ modkey }, "a", function () awful.spawn(mediaplayer) end,
    {description = "run media player", group = "launcher"}),

  -- dmenu
  awful.key({ modkey }, "x", function ()
    awful.spawn(string.format("dmenu_run -i -fn 'Monospace' -nb '%s' -nf '%s' -sb '%s' -sf '%s'",
    beautiful.bg_normal, beautiful.fg_normal, beautiful.bg_focus, beautiful.fg_focus)) end,
    {description = "show dmenu", group = "launcher"}),

  -- Prompt
  awful.key({ modkey }, "r", function () awful.screen.focused().mypromptbox:run() end,
    {description = "run prompt", group = "launcher"})

  --[[ awful.key({ modkey }, "x",
    function ()
    awful.prompt.run {
    prompt       = "Run Lua code: ",
    textbox      = awful.screen.focused().mypromptbox.widget,
    exe_callback = awful.util.eval,
    history_path = awful.util.get_cache_dir() .. "/history_eval"
    }
    end,
    {description = "lua execute prompt", group = "awesome"})
    --]]
)

clientkeys = my_table.join(
  awful.key({ modkey,           }, "f",
    function (c)
      c.fullscreen = not c.fullscreen
      c:raise()
    end,
    {description = "toggle fullscreen", group = "client"}),
  awful.key({ modkey, "Shift"   }, "c",      function (c) c:kill()                         end,
    {description = "close", group = "client"}),
  awful.key({ modkey, "Control" }, "space",  awful.client.floating.toggle                     ,
    {description = "toggle floating", group = "client"}),
  awful.key({ modkey, "Control" }, "Return", function (c) c:swap(awful.client.getmaster()) end,
    {description = "move to master", group = "client"}),
  awful.key({ modkey,           }, "o",      function (c) c:move_to_screen()               end,
    {description = "move to screen", group = "client"}),
  awful.key({ modkey,           }, "t",      function (c) c.ontop = not c.ontop            end,
    {description = "toggle keep on top", group = "client"}),
  awful.key({ modkey,           }, "n",
    function (c)
      -- The client currently has the input focus, so it cannot be
      -- minimized, since minimized clients can't have the focus.
      c.minimized = true
    end ,
    {description = "minimize", group = "client"}),
  awful.key({ modkey,           }, "m",
    function (c)
      c.maximized = not c.maximized
      c:raise()
    end ,
    {description = "maximize", group = "client"})
)

-- Bind all key numbers to tags.
-- Be careful: we use keycodes to make it works on any keyboard layout.
-- This should map on the top row of your keyboard, usually 1 to 9.
for i = 1, 9 do
  -- Hack to only show tags 1 and 9 in the shortcut window (mod+s)
  local descr_view, descr_toggle, descr_move, descr_toggle_focus
  if i == 1 or i == 9 then
    descr_view = {description = "view tag #", group = "tag"}
    descr_toggle = {description = "toggle tag #", group = "tag"}
    descr_move = {description = "move focused client to tag #", group = "tag"}
    descr_toggle_focus = {description = "toggle focused client on tag #", group = "tag"}
  end
  globalkeys = my_table.join(globalkeys,
  -- View tag only.
  awful.key({ modkey }, "#" .. i + 9,
  function ()
    local screen = awful.screen.focused()
    local tag = screen.tags[i]
    if tag then
      tag:view_only()
    end
  end,
  descr_view),
  -- Toggle tag display.
  awful.key({ modkey, "Control" }, "#" .. i + 9,
  function ()
    local screen = awful.screen.focused()
    local tag = screen.tags[i]
    if tag then
      awful.tag.viewtoggle(tag)
    end
  end,
  descr_toggle),
  -- Move client to tag.
  awful.key({ modkey, "Shift" }, "#" .. i + 9,
  function ()
    if client.focus then
      local tag = client.focus.screen.tags[i]
      if tag then
        client.focus:move_to_tag(tag)
      end
    end
  end,
  descr_move),
  -- Toggle tag on focused client.
  awful.key({ modkey, "Control", "Shift" }, "#" .. i + 9,
  function ()
    if client.focus then
      local tag = client.focus.screen.tags[i]
      if tag then
        client.focus:toggle_tag(tag)
      end
    end
  end,
  descr_toggle_focus)
  )
end

clientbuttons = my_table.join(
  awful.button({ }, 1, function (c) client.focus = c; c:raise() end),
  awful.button({ modkey }, 1, awful.mouse.client.move),
  awful.button({ modkey, "Control" }, 1, awful.mouse.client.move),
  awful.button({ modkey }, 3, awful.mouse.client.resize),
  awful.button({ modkey, "Control" }, 3, awful.mouse.client.resize) )

-- Set keys
root.keys(globalkeys)
-- }}}

-- {{{ Rules
-- Rules to apply to new clients (through the "manage" signal).
awful.rules.rules = {
    -- All clients will match this rule.
  { rule = { }, properties = {
      border_width = beautiful.border_width,
      border_color = beautiful.border_normal,
      focus = awful.client.focus.filter,
      raise = true,
      keys = clientkeys,
      buttons = clientbuttons,
      screen = awful.screen.preferred,
      placement = awful.placement.no_overlap+awful.placement.no_offscreen,
      size_hints_honor = false
    }
  },

  -- Titlebars
  { rule_any = { type = { "dialog", "normal" } },
    properties = { titlebars_enabled = false } },

  -- Set Firefox to always map on the first tag on screen 1.
  --{ rule = { class = "Firefox" },
  --  properties = { screen = 1, tag = awful.util.tagnames[1] } },
}
-- }}}

-- {{{ Signals
-- Signal function to execute when a new client appears.
client.connect_signal("manage", function (c)
  -- Set the windows at the slave,
  -- i.e. put it at the end of others instead of setting it master.
  -- if not awesome.startup then awful.client.setslave(c) end

  if awesome.startup and
    not c.size_hints.user_position
    and not c.size_hints.program_position then
    -- Prevent clients from being unreachable after screen count changes.
    awful.placement.no_offscreen(c)
  end
end)

-- Add a titlebar if titlebars_enabled is set to true in the rules.
client.connect_signal("request::titlebars", function(c)
  -- Custom
  if beautiful.titlebar_fun then
    beautiful.titlebar_fun(c)
    return
  end

  -- Default
  -- buttons for the titlebar
  local buttons = my_table.join(
    awful.button({ }, 1, function()
      client.focus = c
      c:raise()
      awful.mouse.client.move(c)
    end),
    awful.button({ }, 3, function()
      client.focus = c
      c:raise()
      awful.mouse.client.resize(c)
    end)
  )

  awful.titlebar(c, {size = 16}) : setup {
    { -- Left
      awful.titlebar.widget.iconwidget(c),
      buttons = buttons,
      layout  = wibox.layout.fixed.horizontal
    },
    { -- Middle
      { -- Title
        align  = "center",
        widget = awful.titlebar.widget.titlewidget(c)
      },
      buttons = buttons,
      layout  = wibox.layout.flex.horizontal
    },
    { -- Right
      awful.titlebar.widget.floatingbutton (c),
      awful.titlebar.widget.maximizedbutton(c),
      awful.titlebar.widget.stickybutton   (c),
      awful.titlebar.widget.ontopbutton    (c),
      awful.titlebar.widget.closebutton    (c),
      layout = wibox.layout.fixed.horizontal()
    },
    layout = wibox.layout.align.horizontal
  }
end)

-- Enable sloppy focus, so that focus follows mouse.
client.connect_signal("mouse::enter", function(c)
  if awful.layout.get(c.screen) ~= awful.layout.suit.magnifier
    and awful.client.focus.filter(c) then
    client.focus = c
  end
end)

-- No border for maximized clients
function border_adjust(c)
  if c.maximized then -- no borders if only 1 client visible
    c.border_width = 0
  elseif #awful.screen.focused().clients > 1 then
    c.border_width = beautiful.border_width
    c.border_color = beautiful.border_focus
  end
end

client.connect_signal("focus", border_adjust)
client.connect_signal("property::maximized", border_adjust)
client.connect_signal("unfocus", function(c) c.border_color = beautiful.border_normal end)

run_once({'/usr/bin/xkbcomp -w 0 ~/.mykeymap.xkb $DISPLAY'})
run_once({'/usr/bin/xset s off'})
run_once({'/usr/bin/nm-applet'})
run_once({'/usr/bin/blueman-applet'})
run_once({'/usr/bin/redshift'})
run_once({'/usr/bin/xscreensaver -no-splash'})
run_once({'/usr/bin/xset dpms 0 0 0'})
