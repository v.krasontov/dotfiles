--- Separating Multiple Monitor functions as a separeted module (taken from awesome wiki)

local gtable  = require("gears.table")
local spawn   = require("awful.spawn")
local naughty = require("naughty")

-- A path to a fancy icon
local icon_path = ""

-- Get active outputs
local function outputs()
  local outputs = {}
  local xrandr = io.popen("xrandr -q --current")

  if xrandr then
    for line in xrandr:lines() do
      local output = line:match("^([%w.-]+) connected ")
      if output then
        outputs[#outputs + 1] = output
      end
    end
    xrandr:close()
  end

  return outputs
end

local function printt(t)
  local text = "["
  for i, v in pairs(t) do
    text = text .. tostring(v)
    if i < #(t) then
      text = text .. ", "
    end
  end
  text = text .. "]"
  return text
end

local function arrange(out)
   -- We need to enumerate all permutations of horizontal outputs.

   local choices  = {}
   local previous = { {} }
   for i = 1, #out do
      -- Find all permutation of length `i`: we take the permutation
      -- of length `i-1` and for each of them, we create new
      -- permutations by adding each output at the end of it if it is
      -- not already present.
      local new = {}
      for _, p in pairs(previous) do
         for _, o in pairs(out) do
            if not gtable.hasitem(p, o) then
               new[#new + 1] = gtable.join(p, {o})
            end
         end
      end
      choices = gtable.join(choices, new)
      previous = new
   end

   return choices
end

local function arrange_many_selectively(out)
  local laptop = 'eDP-1'
  local hdmi = 'HDMI-1'
  local arrangements = {}

  -- build permutations without laptop
  local laptop_index = 0
  for index, screen in pairs(out) do
    if screen == laptop then
      laptop_index = index
    end
  end
  if laptop_index > 0 then
    table.remove(out, laptop_index)
  end
  -- hdmi is always first if it's present
  local hdmi_index = 0
  for index, screen in pairs(out) do
    if screen == hdmi then
      hdmi_index = index
    end
  end
  if hdmi_index > 0 then
    table.remove(out, hdmi_index)
  end
  -- build permutations of remaining outs, with hdmi first and no laptop
  -- either one or two outputs left!
  local hdmi_stub = {}
  if hdmi_index > 0 then
    hdmi_stub = gtable.join(hdmi_stub, {hdmi})
  end
  table.insert(arrangements, gtable.join({}, hdmi_stub, out))
  if #(out) > 1 then
    table.insert(arrangements, gtable.join({}, hdmi_stub, {out[2], out[1]}))
  end

  -- add laptop on left and right side of built permutations
  local laptop_arrangements = {}
  if laptop_index > 0 then
    for _, perm in pairs(arrangements) do
      table.insert(laptop_arrangements, gtable.join({laptop}, perm))
    end
    for _, perm in pairs(arrangements) do
      table.insert(laptop_arrangements, gtable.join(perm, {laptop}))
    end
  end
  naughty.notify{text=tostring(#(laptop_arrangements))}
  arrangements = gtable.join(arrangements, laptop_arrangements)

  -- add laptop alone as first option
  table.insert(arrangements, 1, {laptop})
  return arrangements
end


local function arrange_many(out)
  local laptop_screen = 'eDP-1'
  -- get all permutations
  all_perms = arrange(out)
  -- filter the ones with length < length(out)
  local long_perms = {}
  -- get all perms without laptop screen first
  for _, perm in pairs(all_perms) do
    if #(perm) == #(out) -1 then
      -- do we have the laptop screen in the perm?
      local laptop_screen_included = false
      for _, screen in pairs(perm) do
        if screen == laptop_screen then
          laptop_screen_included = true
        end
      end
      -- laptop screen not included - insert perm at beginning
      if not laptop_screen_included then
        table.insert(long_perms, 1, perm)
      end
    end
    if #(perm) == #(out) then
      -- all screens included - append
      table.insert(long_perms, perm)
    end
  end
  -- add all permutations with all screens
  for _, perm in pairs(all_perms) do
    if #(perm) == #(out) then
      long_perms[#long_perms + 1] = perm
    end
  end
  -- insert only laptop screen
  table.insert(long_perms, {laptop_screen})
  return long_perms
end

-- Build available choices
local function menu()
   local menu = {}
   local out = outputs()
   -- if number of outputs is greater than two, build shorter list of outputs!
   local number_of_outputs = #(out)
   local choices = {}
   if number_of_outputs > 2 then
     choices = arrange_many(out)
   else
     choices = arrange(out)
   end

   for _, choice in pairs(choices) do
      local cmd = "xrandr"
      -- Enabled outputs
      for i, o in pairs(choice) do
         cmd = cmd .. " --output " .. o .. " --auto"
         if i > 1 then
            cmd = cmd .. " --right-of " .. choice[i-1]
         end
      end
      -- Disabled outputs
      for _, o in pairs(out) do
         if not gtable.hasitem(choice, o) then
            cmd = cmd .. " --output " .. o .. " --off"
         end
      end

      local label = ""
      if #choice == 1 then
         label = 'Only <span weight="bold">' .. choice[1] .. '</span>'
      else
         for i, o in pairs(choice) do
            if i > 1 then label = label .. " + " end
            label = label .. '<span weight="bold">' .. o .. '</span>'
         end
      end

      menu[#menu + 1] = { label, cmd }
   end

   return menu
end

-- Display xrandr notifications from choices
local state = { cid = nil }

local function naughty_destroy_callback(reason)
  if reason == naughty.notificationClosedReason.expired then
    local action = state.index and state.menu[state.index - 1][2]
    if action then
      spawn(action, false)
      state.index = nil
    end
  end
  if reason == naughty.notificationClosedReason.dismissedByUser then
    state.index = nil
  end
end

local function xrandr()
  -- Build the list of choices
  if not state.index then
     state.menu = menu()
     state.index = 1
  end

  -- local test1 = {"eDP-1", "HDMI-1", "DIV1", "DIV2"}
  -- local choices = arrange_many_selectively(test1)

  -- Select one and display the appropriate notification
  local label, action
  local next  = state.menu[state.index]
  state.index = state.index + 1

  if not next then
     label = "Keep the current configuration"
     state.index = nil
  else
     label, action = next[1], next[2]
  end
  state.cid = naughty.notify({ text = label,
                               icon = icon_path,
                               timeout = 4,
                               screen = mouse.screen,
                               replaces_id = state.cid,
                               destroy = naughty_destroy_callback}).id
end

return {
   outputs = outputs,
   arrange = arrange,
   menu = menu,
   xrandr = xrandr
}
