local ipairs, string, os, table, tostring, tonumber, type = ipairs, string, os, table, tostring, tonumber, type
local gears         = require("gears")
local gtable  = require("gears.table")

local function arrange_many_selectively(out)
  local laptop = 'eDP-1'
  local hdmi = 'HDMI-1'
  local arrangements = {}

  -- build permutations without laptop
  local laptop_index = 0
  for index, screen in pairs(out) do
    if screen == laptop then
      laptop_index = index
    end
  end
  if laptop_index > 0 then
    table.remove(out, laptop_index)
  end
  -- hdmi is always first if it's present
  local hdmi_index = 0
  for index, screen in pairs(out) do
    if screen == hdmi then
      hdmi_index = index
    end
  end
  if hdmi_index > 0 then
    table.remove(out, hdmi_index)
  end
  -- build permutations of remaining outs, with hdmi first and no laptop
  -- either one or two outputs left!
  local hdmi_stub = {}
  if hdmi_index > 0 then
    hdmi_stub = gtable.join(hdmi_stub, {hdmi})
  end
  table.insert(arrangements, gtable.join({}, hdmi_stub, out))
  if #(out) > 1 then
    table.insert(arrangements, gtable.join({}, hdmi_stub, {out[2], out[1]}))
  end

  -- add laptop on left and right side of built permutations if laptop
  -- connected
  if laptop_index > 0 then
    for _, perm in pairs(arrangements) do
      table.insert(arrangements, 1, table.insert(perm, laptop))
      table.insert(arrangements, table.insert(perm, laptop))
    end
  end

  -- add laptop alone as first option
  table.insert(arrangements, 1, {laptop})
  return arrangements
end

test1 = {"eDP-1", "HDMI-1", "DIV1", "DIV2"}

print("hello world")
